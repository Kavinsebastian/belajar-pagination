### apa itu pagination/paging


 merupakan teknik untuk menampilkan data pada tabel kedalam beberapa halaman. Misalnya saya memiliki 100 data, nah ketika saya tampilkan seluruh data tesebut maka halaman untuk menampilkan data harus dicroll sampai kebawah, dan tentunya hal ini kurang efektif. Dengan menggunakan pagination maka data tersebut bisa dibagi dalam beberapa halaman tergantung kita mau menampilkan berapa data dalam satu halaman.


 sumber referensi di [malasngoding](https://www.malasngoding.com/cara-membuat-pagination-php-mysqli-dan-boostrap-4/)
