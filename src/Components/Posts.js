import React from 'react'
import styled from 'styled-components'
export default function Posts({posts, loading}) {

    const Li = styled.li`
        list-style:none;
        padding:10px;
        border:4px solid rgba(0,0,0,0.1);
        transition: 1s;
        &:hover{
            background-color:#444;
            color:white;
            cursor: pointer;
        }
    `;
    
    if(loading){
        return <h1>Loading ..</h1>
    }
    
    return (
        <div>
            <ul>
            {
                posts.map(post => (
                    <Li key={post.id}>
                        {post.title}
                    </Li>
                ))
            }
            </ul>
        </div>
    )
}
