import React from 'react'
import styled from 'styled-components'

export default function Pagination({postPerPage, totalPosts, paginate}) {

    const Ul = styled.li`
        display:flex;
        `;
    const Li = styled.li`
        list-style:none;
        padding:15px;
    `;
    const Link = styled.a`
        text-decoration:none;
        color:black;
    `;

    const pageNumber = []
    for(let i = 1; i <= Math.ceil(totalPosts/postPerPage); i++){
        pageNumber.push(i)
    }

    return (
        <nav>
            <Ul>
                {pageNumber.map(number => (
                    <Li key={number}>
                        <Link onClick={() => paginate(number)} href="!#">{number}</Link>
                        </Li>
                ))}
            </Ul>
        </nav>
    )
}
