import './App.css';
import axios from 'axios';
import { useEffect, useState } from 'react';
import Posts from './Components/Posts';
import Pagination from './Components/Pagination';
import styled from 'styled-components'


function App() {
  const [posts, setPosts] = useState([]);
  const [loading, setLoading] = useState(false);
  const [currentPage, setCurrentPage] = useState(2);
  const [postPerPage] = useState(10);


  useEffect(() => {
    setLoading(true)
    fetchData()
    setLoading(false)
  },[])

  const fetchData = async() => {
    await axios.get('https://jsonplaceholder.typicode.com/posts')
    .then(res => {
      setPosts(res.data)
    })
    .catch(err => console.log(err))
  }

// Get current post 
const indexOfLastPost = currentPage * postPerPage;
const indexOfFirstPost = indexOfLastPost - postPerPage
const currentPost = posts.slice(indexOfFirstPost, indexOfLastPost)

// change page
const paginate = (pageNumber) => setCurrentPage(pageNumber)



// style

const TextHeader = styled.h1`
  font-size:3rem;
  color:blue;
`;

const Content = styled.div`
  width:500px;
  height:auto;
  margin:auto;
  text-align:start;
`;


  return (
    <Content>
      <TextHeader>My Blog</TextHeader>
      <Posts posts={currentPost} loading={loading}/>
      <Pagination totalPosts={posts.length} postPerPage={postPerPage} paginate={paginate}/>
    </Content>
  );
}

export default App;
